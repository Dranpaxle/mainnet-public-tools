# How to earn extra Q Tokens

## Setup MetaMask and using Your HQ Website

On Q network, you are able to earn extra Q tokens for participating in the Q governance. As a basic requirement you must use a wallet application that carries some tokens. We suggest to use MetaMask. There are 2 options to earn tokens. The first option is to only transfer Q tokens into a Q vault. The second option is to earn a reward on Q synthetic asset (QUSD) which is more complex but offers more benefits for earning extra tokens.

## Setup MetaMask and Your HQ Website

At first, go to page [*Your HQ*](https://hq.q.org/). Complete the MetaMask installation by following the instructions on the website or follow this [tutorial](how-to-install-metamask.md) to setup your MetaMask account.

## Option I: Q Vault

After connecting MetaMask, go to the *Q Vault* tab. If you happen to encounter the term *Piggy Bank* please note that the terms *Piggy Bank* and *Q vault* are referring to the very same functionality and are therefore redundant.
You will see your Q vault balance and Q address balance. In Q vault balance you can see your Q balance within your individual Q vault, whereas Q address balance shows your Q balance on your MetaMask wallet account. Note that your voting weight is zero if you use the Q vault for the first time.

To fund your Q vault balance, enter the fundable amount into the input field *Transfer into Q vault* and click *Transfer*. Once you want to withdraw funds back to your MetaMask wallet, you need to do the same operation with input field *Withdraw from Q vault*.
Depending on system-widely set parameter *Q token holder reward rate*, you will now earn a reward on your Q tokens that are locked within your Q vault.

## Option II: Saving in Q Synthetic Asset (QUSD)

You must have QUSD tokens within your wallet which can be obtained in different ways:

- Provide digital assets such as QBTC as collateral in a decentralized borrowing transaction (see here) and receive freshly minted QUSD.
- Buy QUSD from external resources and participating network partners, e.g. exchanges or merchants accepting QUSD as a payment method.
- Accept payments in QUSD for selling goods or services.

Within *Your HQ*, go to *Saving and Borrowing* tab and click on section *Saving Crypto Assets*. The sidebar showing up includes relevant information on asset type and balances as well as saving reward and earnings. Depending on how many QUSD you have in your MetaMask wallet, you can deposit the according amount into the saving contract after clicking on *Deposit Saving Asset* and entering the amount. If you want to withdraw from saving contract, you need to use the button *Withdraw Saving Asset* accordingly.
