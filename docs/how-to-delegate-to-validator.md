# How to delegate Q to a Validator

## Intro

When you have Q tokens, you can delegate them to another validator node that you trust. Q delegation increases a validator's accountable stake and moves him up the validator long list. The validators with the most stake are members of the short list and actively participate in Q by building blocks according to the clique algorithm. Validators receive a reward for block generation. This reward is split and distributed to any potential stake delegator according to a ratio predefined by the validator node operator. This way, a Q token holder can participate in earning block rewards without actively operating a validator node.

## Your HQ and MetaMask

The easiest way to delegate your stake is to use our dApp *Your HQ*. One instance is hosted at [https://hq.q.org](https://hq.q.org). You will need to have a running MetaMask wallet plug-in before being able to interact with the relevant delegation smart contract through *Your HQ*. Please check the [MetaMask tutorial](how-to-install-metamask.md) first.

## Delegation

After connecting MetaMask to *Your HQ*, click on "Q Vault" to enter the Q Vault area. On the bottom of the page, you can see your current delegations and you can enter the validator nodes address and Q amount you want to delegate.
