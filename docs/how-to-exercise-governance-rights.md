# How to exercise your Governance Rights as Q Token Holder

## Your HQ and MetaMask

The easiest way to make use of the Q governance features is to use our dApp *Your HQ*. One instance is hosted at [https://hq.q.org/](https://hq.q.org/). You will need to have a running MetaMask wallet plug-in before being able to interact with the relevant delegation smart contract through *Your HQ*. Please check the MetaMask tutorial first.

## Account Funding

After connecting *Your HQ* with MetaMask, go to the *Q Vault* area. You will see your active parameters Q vault balance and Q address balance. In *Q Vault balance* you can see your Q balance on the platform, and in Q address balance you see the display of your Q balance on the address (wallet) in MetaMask. Observe also that your voting weight is zero if you use the Q vault for the first time.

To replenish your Q vault balance check the manage balance sub-tab. Lookout for the *Transfer into Q Vault* input field, enter your desired amount of Q to transfer, and click on *Transfer*. When withdrawing funds back to your MetaMask Wallet, do the same operation in the line *Withdraw from Q Vault*.

## Locking Funds for Voting

Now with Q on the platform, you need to lock the number of tokens that you plan to vote with. In other words, the number of locked tokens will set your voting weight. This strategy prevents double-vote scenarios. In the Lock your Coins for Voting sub-tab, enter the number of coins to be locked. If you already have a locked balance, you can increase the number of locked coins, or reduce the amount to get back control over the Q. Note that in order to participate in proposals, your funds will always be locked until the proposal voting phase is over.

## Delegating Voting Power

It is also possible to let a representative vote in your place by delegating your locked funds to their address. Your representative's voting weight will then be increased by the amount of Q you locked for voting.  Similar to voting by yourself, your funds will also be locked until the proposal voting phase is over if your representative casts his vote. The representative will have no access to your funds.

To delegate your voting power, go to the *Q Vault* area and enter the wallet address of your representative in the *Delegate Voting Power* section and click on *Announce* and confirm the MetaMask transaction. The address of your representative will appear under *Current agent*. You have to click on the *confirm* button and confirm another MetaMask transaction before the delegation takes effect.

## Governance

Now go to the *Q Governance* page and select the required voting section:

- Q proposal constitution update
- General Q update, emergency update (available only for root nodes)
- Q root node panel (add a new root node,remove a current root node)
- Q expert proposal (add a new expert, remove a current expert, parameter vote)
- Slashing proposal (slash a root node or slash a validator node)

On the page of any proposal type, you can create new proposals on the top right or inspect and vote for active proposals. Further, you can also find past proposals below in the ended proposals section.  
The vote for each proposal type is very similar. First you have a basic vote on proposal which constitutes a simple yes / no vote for the given proposal. Second, there is the constitution check which allows root nodes to place a Veto for the given proposal. Note that certain votes can only be conducted by certain stakeholders. For example, a DeFi risk parameter can only be proposed and voted on by DeFi risk experts.
