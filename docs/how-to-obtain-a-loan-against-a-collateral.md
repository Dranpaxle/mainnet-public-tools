# How to obtain a Loan against a Collateral

## Setup MetaMask and Your HQ

On the Q network, you are able to take a loan in the QUSD against collateral in QBTC and use these assets in your aims. To do this you  must have a MetaMask account and QBTC in the wallet.

First, go to the dApp [*Your HQ*](https://hq.q.org/). Complete the installation by following the instructions on the website or follow this [tutorial](how-to-install-metamask.md) to setup your MetaMask account.

## Account Funding

You must have Q and QBTC tokens within your wallet. There are several options how you can achieve this:

- Buy QBTC from someone else, e.g. on an exchange
- Provide goods or services and accept QBTC as payment asset
- Receive tokens from someone else, e.g. friends & families

## QBTC Vault

To start go to the *Saving and Borrowing* tab within Your HQ website mentioned in step 1. Create a new QBTC vault by clicking on the appropriate button. After successful signing of the according transaction via MetaMask and its successful confirmation, you can now see the newly created vault and interact with it by clicking on it. You will see two sections in the right sidebar: Collateral and Borrowing.

- *Collateral* shows all information about your locked QBTC. The asset price is the actual price of BTC on the exchange. The liquidation price is based on your borrowing value.

- *Borrowing* stores all parameters about received QUSD. Outstanding debt is a value of QUSD which you borrow with the fee. Liquidation limit is a minimum safe rate of the asset price.
Note: if the asset price falls below the liquidation limit, your collateral will be put up on the decentralized auctions.

## Borrowing

First, you need to deposit QBTC in the *Borrow QUSD* section. You will see that the available deposit value is always smaller than the collateral value. Next, select to borrow several assets that you want to take out in a loan. In consequence, an equal part of the collateral will be locked. You can use the loan how you want, but remember that outstanding debt will be increasing every day, on the proportionality of the borrowing fee per year.
