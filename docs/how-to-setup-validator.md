# How to Setup a Q Validator Node

## Setup your Server

You must prepare your server / machine to begin. One possibility is to use a local machine, alternatively you can use a cloud instance on AWS for example. There is a good external tutorial on how to get started with Ethereum on AWS. You can use this [tutorial](https://medium.com/@pilankar.akshay3/how-to-setup-a-ethereum-poa-private-proof-of-authority-ethereum-network-network-on-amazon-aws-5fdf56d2ad93) as a basic reference.

## Basic Configuration

Clone the repository `https://gitlab.com/q-dev/mainnet-public-tools` and go to the `/validator` directory. This directory contains the `docker-compose.yaml` file for quick launching of full node with enabled rpc along with blockchain explorer, `.env` file for ports configuration and `genesis.json` with the configuration of genesis block of Q mainnet.

## Generate a Keypair for Validator

In order to sign blocks and receive reward, a validator needs a keypair.
Create a `/keystore` directory, then a password which will be used for private key encryption and save it into a text file `pwd.txt` in `/keystore` directory.
Assuming you are in `/validator` directory, issue this command in order to generate a keypair:  

```text
$ docker run --entrypoint="" --rm -v $PWD:/data -it qblockchain/q-client:mainnet geth account new --datadir=/data --password=/data/keystore/pwd.txt
```

The output of this command should look like this:

```text
Your new key was generated

Public address of the key:   0xb3FF24F818b0ff6Cc50de951bcB8f86b52287dac
Path of the secret key file: /data/keystore/UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac

- You can share your public address with anyone. Others need it to interact with you.
- You must NEVER share the secret key with anyone! The key controls access to your funds!
- You must BACKUP your key file! Without the key, it's impossible to access account funds!
- You must REMEMBER your password! Without the password, it's impossible to decrypt the key!
```

This way, a new private key is generated and stored in keystore directory encrypted with password from pwd.txt file. In our example, *0xb3FF24F818b0ff6Cc50de951bcB8f86b52287DAc* (**you will have a different value**) is the address corresponding to the newly generated private key.

Alternatively, you can generate a secret key pair and according file on this [page](https://vanity-eth.tk/) and save it to the `/keystore` directory manually.
Also you may use `create-geth-private-key.js` script in `/js-tools` folder.

Whether you chose to provide your own vanity keys or use the above command to create a keypair, please ensure that the directory `/keystore` contains the following files:

```text
validator
|   ...
|   ...
└ keystore
  |   UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac
  |   pwd.txt
```

> **Note: ** *Following our example, pwd.txt contains the password to encrypted file "UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac" in clear text.*

If you want to change the password in the future, you need to stop the node first.

```text
$ docker-compose down
```

Then start password reset procedure with

```text
$ docker-compose run validator-node --datadir /data account update 0xb3ff24f818b0ff6cc50de951bcb8f86b52287dac
```

> **Note: ** *You need to remove address _0xb3ff24f818b0ff6cc50de951bcb8f86b52287dac_ and add your account address instead.*

## Get Q Tokens

In order to become a validator, you will need to put some stake in validators contract, so you need Q tokens for this. We are working on a public listing of the Q token at the moment.

## Configure Setup

Edit `.env` file in `/validator` directory. Put your address without leading 0x from the step 3, into `ADDRESS`, your public IP address (please make sure your machine is reachable at the corresponding IP) into `IP` (this is required for discoverability by other network participants) and optionally choose a port for p2p protocol (or just leave default value). The resulting `.env` file should look like this:

```text
# docker image for q client
QCLIENT_IMAGE=qblockchain/q-client:mainnet

# your q address here (without leading 0x)
ADDRESS=b3FF24F818b0ff6Cc50de951bcB8f86b52287DAc

# your public IP address here
IP=193.19.228.94

# the port you want to use for p2p communication (default is 30303)
EXT_PORT=30303

# the initial root node set if never connected before
INITIALROOTS=0xBADA551878e60B7D9173452695c1b3D190c3a3DC,0xFd3ba4c7EbDa55C038316C776F2479b2909da7a5

# only root lists later than this will be considered for updates
ROOTTIMESTAMP=1647418453

# extra bootnode you want to use
BOOTNODE1_ADDR=enode://2e085ed39a57457bbd33f9e9d642eafcd178412b2aaf37866c680bfcfd005503b120c068895df2cd69d194470d244f6d6a5b09bb201771907ecbaa74b3bb73c5@79.125.97.227:30314
BOOTNODE2_ADDR=enode://02e601123249d5d5cf3b25633fee1ee0f42a301240e70091197741f0f0f536bb4a0daf4df404ad5aa9c79dcbea39fb2aa8010dfbb97e179f5f8dc9b5dbd7544c@79.125.97.227:30315
BOOTNODE3_ADDR=enode://38a27ff4f10f544df0ce774b018e9b9b9937fc38900645d28454bd553b3560682978e7b6a2d222127a88e1c1153b8068c46526aa74e2ef3ff0f602de1bcde30f@79.125.97.227:30316
```

## Put Stake in Validators Contract

As was mentioned previously, you need to put stake to validators contract in order to become a validator. You can use the dApp "Your HQ" that can be found at [https://hq.q.org](https://hq.q.org). Ultimately, you need to `Join Validator Ranking` to receive rewards. The according functionality is located at `Consensus Services -> Validator Staking` in box "Manage Balance". If you can't see the menu item `Consensus Services`, you are not running the dApp UI in `advanced mode`. Go to `Settings` and activate it.

## Add your Validator to https://stats.q.org

If you want your validator to report to the [network statistics](https://stats.q.org), you can add an additional flag to the node entrypoint within file `/validator/docker-compose.yaml`, it should look like this:

```text
validator-node:
  image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--ethstats=<Your_Validator_Name>:<Mainnet_access_key>@stats.q.org", "--datadir=/data", ...]
```

`<Your_Validator_Name>` can be chosen arbitrarily. It will be displayed in the statistics. If you want to disclose your ID, this could be something like "OurCoolCompany - Don't trust, verify". You can use special characters, emojis as well as spaces. If you prefer to stay anonymous, we would appreciate to include the beginning of your validator Q address, so there is a link between your client and your address.

In order to find out the `<Mainnet_access_key>` we ask you to write to us [on Discord](https://discord.gg/YTgkvJvZGD).

## Launch Validator Node

Now launch your validator node using docker-compose file in validator directory:

```text
$ docker-compose up -d
```

Note: Check our nodes real-time logs with the following command:

```text
$ docker-compose logs -f --tail "100"
```

## Find additional peers

In case your client can't connect with the default configuration, we recommend that you add an additional flag referring to one of our additional peers (`$BOOTNODE1_ADDR`, `$BOOTNODE2_ADDR`or `$BOOTNODE3_ADDR`) within `docker-compose.yaml` file:

```text
validator-node:
  image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--bootnodes=$BOOTNODE_ADDR", "--datadir=/data", ...]
```

## Verify that Node is producing Blocks

In order for you to start validating, you must wait for the new epoch (i.e. validation cycle). If everything went correctly before and the committed stake was sufficient to enter the validator shortlist, your validator node will start to produce blocks in the next validation cycle.
Please note that upon start you are likely to see a lot of warnings in q-client logs:

```text
WARN [01-18|13:12:00.431] Block sealing failed          err="unauthorized signer"
```

This is actually ok, as the node needs some time to synchronize with the peers of Q network. Until a full sync is reached, it may happen that your node already starts block creation using the most recent snapshot in which you are the only validator. After successful peer discovery, there warnings will disappear.

  > **Note: ** *All validators are required to run an omnibridge-oracle as per Constitution. Please see [here](how-to-setup-omnibridge.md#Configure-OmniBridge-oracle) for a tutorial how to do this.*

## Exit the Validator Ranking

If you want to exit the Validator Ranking, you must `Announce Withdrawal` within `Consensus Services -> Validator Staking` of 100% of your self-staked Q token. After doing that, you will be taken out of the ranking immediately, though your node might still validate blocks until the next validation cycle begins (within max. 8 minutes).

The announced amount will be put on an escrow balance for a certain time (see constitution parameter `constitution.valWithdrawP`) until it can be withdrawn fully. Re-joining the panel is possible any time by putting back stake or reducing the announced withdrawal amount.

  > **Note: ** *A temporary exit from the ranking is possible as described above. For re-entering, you need to announce withdrawal of `0` Q which overwrites your initial announcement and restores your self-stake to 100%. You need to `Join Validator Ranking` again to finalise the re-entering procedure. A temporary exit (or pause) might be useful if you are planning a maintenance downtime of your node for example.*
